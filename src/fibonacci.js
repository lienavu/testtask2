const fibonacciNumbers = (num) => {
  // Your implementation
  // Read README.md file, if you not understand what to do
  if (typeof num === "number") {
    if (num === 0) {
      return 0;
    }
    if (num === 1) {
      return 1;
    }
    return fibonacciNumbers(num - 1) + fibonacciNumbers(num - 2);
  } else {
    return "Passed argument is not a number";
  }
};
module.exports = fibonacciNumbers;
